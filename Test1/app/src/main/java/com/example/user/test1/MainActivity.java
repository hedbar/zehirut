package com.example.user.test1;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {

    WifiP2pManager mManager;
    WifiP2pManager.Channel mChannel;
    BroadcastReceiver mReceiver;
    IntentFilter mIntentFilter;

    private static String cat = MainActivity.class.getName();




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(cat, "onCreate");


        mManager = (WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE);
        mChannel = mManager.initialize(this, getMainLooper(), null);
        mReceiver = new WiFiDirectBroadcastReceiver(mManager, mChannel, this);

        mIntentFilter = new IntentFilter();
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);

        mManager.discoverPeers(mChannel, new WifiP2pManager.ActionListener() {
            @Override
            public void onSuccess() {
                Log.d(cat, "discoverPeers - onSuccess");
            }

            @Override
            public void onFailure(int reasonCode) {
                Log.d(cat, "discoverPeers - onFailure");
            }
        });




        setContentView(R.layout.activity_main);
    }

    /* register the broadcast receiver with the intent values to be matched */
    @Override
    protected void onResume() {
        super.onResume();
        Log.d(cat, "onResume");
        registerReceiver(mReceiver, mIntentFilter);
    }
    /* unregister the broadcast receiver */
    @Override
    protected void onPause() {
        super.onPause();
        Log.d(cat, "onPause");
        unregisterReceiver(mReceiver);
    }



    public void clear_text(View view){
        TextView peers_out = (TextView)this.findViewById(R.id.peers_out);
        peers_out.setText("");
    }

    public void search_peers(View view){
        Log.d(cat, "view = " + String.valueOf(view) );

        final TextView peers_out = (TextView)this.findViewById(R.id.peers_out);
        Log.d(cat, "peers_out = " + String.valueOf(peers_out) );

        WifiP2pManager.PeerListListener myPeerListListener = new WifiP2pManager.PeerListListener() {
            @Override
            public void onPeersAvailable(WifiP2pDeviceList wifiP2pDeviceList) {
                Log.d(cat, "onPeersAvailable");
                Iterator<WifiP2pDevice> iter = wifiP2pDeviceList.getDeviceList().iterator();
                for (;iter.hasNext();) {
                    WifiP2pDevice device = iter.next();
                    String deviceName=device.deviceName;
                    Log.d(cat, "deviceName = " + deviceName + ", address = " + device.deviceAddress);
                    peers_out.append(deviceName + " - " + device.deviceAddress +"\n");

                    // try to connect to peer
                    final WifiP2pConfig config = new WifiP2pConfig();
                    config.deviceAddress = device.deviceAddress;
                    mManager.connect(mChannel, config, new WifiP2pManager.ActionListener() {

                        @Override
                        public void onSuccess() {
                            Log.d(cat, "onSuccess : " + config.deviceAddress);
                            peers_out.append("Connected to " + config.deviceAddress +"\n");

                        }

                        @Override
                        public void onFailure(int reason) {
                            Log.d(cat, "onFailure : " + config.deviceAddress + " reason: " + reason);
                        }
                    });

                }

            }
        };

        // Call WifiP2pManager.requestPeers() to get a list of current peers

        // request available peers from the wifi p2p manager. This is an
        // asynchronous call and the calling activity is notified with a
        // callback on PeerListListener.onPeersAvailable()
        if (mManager != null) {
            mManager.requestPeers(mChannel, myPeerListListener);
        }

    }
}
