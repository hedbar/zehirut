package com.example.android.wifidirect;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import java.nio.channels.SocketChannel;

import static com.example.android.wifidirect.Constants.*;


/**
 * Created by User on 16/09/2016.
 */

public class ConnectionService extends Service {



    
    private static ConnectionService _sinstance = null;
    private  WorkHandler mWorkHandler;
    private  MessageHandler mHandler;
    ConnectionManager mConnMan;
    WiFiDirectApp mApp;
    WiFiDirectActivity mActivity;    // shall I use weak reference here ?

    private static final String TAG = ConnectionService.class.getName();


    public static ConnectionService getInstance(){
        // we assume here we've been initailized
        return _sinstance;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        _initialize();
        Log.i(TAG, "onCreate : done");
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        _initialize();
        processIntent(intent);
        return START_STICKY;
    }


    private void processIntent(Intent intent) {
        if (intent == null) {
            return;
        }
        return;
    }

    private void _initialize() {
        if (_sinstance != null) {
            Log.d(TAG, "_initialize, already initialized, do nothing.");
            return;
        }

        _sinstance = this;
        mWorkHandler = new WorkHandler(TAG);
        mHandler = new MessageHandler(mWorkHandler.getLooper());

        mApp = (WiFiDirectApp)getApplication();
        mApp.mP2pMan = (WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE);
        mApp.mP2pChannel = mApp.mP2pMan.initialize(this, mWorkHandler.getLooper(), null);
        Log.d(TAG, "_initialize, get p2p service and init channel !!!");

        mConnMan = new ConnectionManager(this);
    }
    @Override
    public IBinder onBind(Intent arg0) {
        IBinder ret = null;
        return ret;
    }


    public Handler getHandler() {
        return mHandler;
    }

    private void onActivityRegister(WiFiDirectActivity activity, int register){
        Log.d(TAG, "onActivityRegister : activity register itself to service : " + register);
        if( register == 1){
            mActivity = activity;
        }else{
            mActivity = null;    // set to null explicitly to avoid mem leak.
        }
    }


    /**
     * the main message process loop.
     */
    private void processMessage(android.os.Message msg) {

        switch (msg.what) {
            case MSG_NULL:
                break;
            case MSG_REGISTER_ACTIVITY:
                Log.d(TAG, "processMessage: onActivityRegister to chat fragment...");
                onActivityRegister((WiFiDirectActivity)msg.obj, msg.arg1);
                break;
            case MSG_STARTSERVER:
                Log.d(TAG, "processMessage: startServerSelector...");
                if( mConnMan.startServerSelector() >= 0){
                    //enableStartChatActivity();
                }
                break;
            case MSG_STARTCLIENT:
                Log.d(TAG, "processMessage: startClientSelector...");
                if( mConnMan.startClientSelector((String)msg.obj) >= 0){
                  //  enableStartChatActivity();
                }
                break;
            case MSG_NEW_CLIENT:
                Log.d(TAG, "processMessage:  onNewClient...");
                mConnMan.onNewClient((SocketChannel)msg.obj);
                break;
            case MSG_FINISH_CONNECT:
                Log.d(TAG, "processMessage:  onFinishConnect...");
                mConnMan.onFinishConnect((SocketChannel)msg.obj);
                break;
            case MSG_PULLIN_DATA:
                Log.d(TAG, "processMessage:  onPullIndata ...");
                onPullInData((SocketChannel)msg.obj, msg.getData());
                break;
            case MSG_PUSHOUT_DATA:
                Log.d(TAG, "processMessage: onPushOutData...");
                onPushOutData((String)msg.obj);
                break;
            case MSG_SELECT_ERROR:
                Log.d(TAG, "processMessage: onSelectorError...");
                mConnMan.onSelectorError();
                break;
            case MSG_BROKEN_CONN:
                Log.d(TAG, "processMessage: onBrokenConn...");
                mConnMan.onBrokenConn((SocketChannel)msg.obj);
                break;
            default:
                break;
        }
    }


    public void onConnectionInfoAvailable(final WifiP2pInfo info) {
        Log.d(TAG, "onConnectionInfoAvailable: " + info.groupOwnerAddress.getHostAddress());
        if (info.groupFormed && info.isGroupOwner ) {
            // XXX server path goes to peer connected.
            //new FileServerAsyncTask(getActivity(), mContentView.findViewById(R.id.status_text)).execute();
            //PTPLog.d(TAG, "onConnectionInfoAvailable: device is groupOwner: startSocketServer ");
            // mApp.startSocketServer();
        } else if (info.groupFormed) {
            Log.d(TAG, "onConnectionInfoAvailable: device is client, connect to group owner: startSocketClient ");
            mApp.startSocketClient(info.groupOwnerAddress.getHostAddress());
        }
        mApp.mP2pConnected = true;
        mApp.mP2pInfo = info;   // connection info available
    }
    /**
     * service handle data in come from socket channel
     */
    private String onPullInData(SocketChannel schannel, Bundle b){
        String data = b.getString("DATA");
        Log.d(TAG, "onDataIn : recvd msg : " + data);
        mConnMan.onDataIn(schannel, data);  // pub to all client if this device is server.
        MessageRow row = MessageRow.parseMessageRow(data);
        // now first add to app json array
        mApp.shiftInsertMessage(row);
        Log.i(TAG,"showNotification(row)");
        // add to activity if it is on focus.
        Log.i(TAG,"showInActivity(row)");
        return data;
    }

    /**
     * handle data push out request.
     * If the sender is the server, pub to all client.
     * If the sender is client, only can send to the server.
     */
    private void onPushOutData(String data){
        Log.d(TAG, "onPushOutData : " + data);
        mConnMan.pushOutData(data);
    }

    /**
     * message handler looper to handle all the msg sent to location manager.
     */
    final class MessageHandler extends Handler {
        public MessageHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            processMessage(msg);
        }
    }

}
