package com.example.android.wifidirect;

import android.app.Application;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.Message;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import static com.example.android.wifidirect.Constants.*;
/**
 * Created by User on 16/09/2016.
 */
public class WiFiDirectApp extends Application {

    private static final String TAG = WiFiDirectApp.class.getName();

    String mMyAddr = null;
    JSONArray mMessageArray = new JSONArray();

    boolean mIsServer = false;
    WifiP2pManager mP2pMan = null;;
    WifiP2pManager.Channel mP2pChannel = null;
    boolean mP2pConnected = false;
    WifiP2pInfo mP2pInfo = null;  // set when connection info available, reset when WIFI_P2P_CONNECTION_CHANGED_ACTION

    WiFiDirectActivity mHomeActivity = null;

    WifiP2pDevice mCurrentDevice= null;



    public void setMyAddr(String addr){
        mMyAddr = addr;
    }



    public void clearMessages() {
        mMessageArray = new JSONArray();
    }


    public void shiftInsertMessage(String jsonmsg){
        JSONObject jsonobj = JSONUtils.getJsonObject(jsonmsg);
        mMessageArray.put(jsonobj);
        mMessageArray = JSONUtils.truncateJSONArray(mMessageArray, 10);  // truncate the oldest 10.
    }

    public String shiftInsertMessage(MessageRow row) {
        JSONObject jsonobj = MessageRow.getAsJSONObject(row);
        if( jsonobj != null ){
            mMessageArray.put(jsonobj);
        }
        mMessageArray = JSONUtils.truncateJSONArray(mMessageArray, 10);  // truncate the oldest 10.
        return jsonobj.toString();
    }


    /**
     * upon p2p connection available, non group owner start socket channel connect to group owner.
     */
    public void startSocketClient(String hostname) {
        Log.d(TAG, "startSocketClient : client connect to group owner : " + hostname);
        Message msg = ConnectionService.getInstance().getHandler().obtainMessage();
        msg.what = MSG_STARTCLIENT;
        msg.obj = hostname;
        ConnectionService.getInstance().getHandler().sendMessage(msg);
    }

}
