package com.zehirut.user.test2;

import org.jdeferred.AlwaysCallback;
import org.jdeferred.Deferred;
import org.jdeferred.DoneCallback;
import org.jdeferred.FailCallback;
import org.jdeferred.ProgressCallback;
import org.jdeferred.Promise;
import org.jdeferred.impl.DeferredObject;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class DeferredUnitTest {
    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void deferred1() throws Exception {
        Deferred deferred = new DeferredObject();
        Promise promise = deferred.promise();
        promise.done(new DoneCallback() {
            public void onDone(Object result) {
                assert "resolve".equals(result);
            }
        }).fail(new FailCallback() {
            public void onFail(Object rejection) {
                assert false;
            }
        }).progress(new ProgressCallback() {
            public void onProgress(Object progress) {
                assert false;
            }
        }).always(new AlwaysCallback() {
            public void onAlways(Promise.State state, Object result, Object rejection) {
                assert Promise.State.RESOLVED.equals(state);
            }
        });

        deferred.resolve("resolve");

    }
}