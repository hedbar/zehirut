package com.zehirut.user.test2;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import static com.zehirut.user.test2.Constants.*;

public class PlayerChoosingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player_choosing);


        SharedPreferences settings = getSharedPreferences(MAIN_PREFERNCE_FILE,Context.MODE_PRIVATE);
        String gameName = settings.getString(F_GAME_NAME,getString(R.string.default_game_name));
        ((TextView)findViewById(R.id.Activity_Player_Choosing)).setText(gameName);





    }
}
