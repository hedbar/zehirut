package com.zehirut.user.test2;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.wifi.p2p.WifiP2pManager;
import android.net.wifi.p2p.nsd.WifiP2pDnsSdServiceInfo;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import org.jdeferred.DoneCallback;
import org.jdeferred.FailCallback;
import org.jdeferred.ProgressCallback;
import org.jdeferred.Promise;
import org.jdeferred.impl.DeferredObject;

import static com.zehirut.user.test2.Constants.*;

import java.util.HashMap;

public class FirstActivity extends AppCompatActivity {

    private static final String TAG = FirstActivity.class.getName();
    private Manager manager = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);
        final FirstActivity theCurrentActivity = this;

        manager = new Manager(this);
        manager.init();



        findViewById(R.id.open_new_game).setOnClickListener(new View.OnClickListener() {
                                                                  @Override
                                                                  public void onClick(View view) {


                                                                      String gameName = ((EditText)findViewById(R.id.GameNameEntry)).getText().toString();
                                                                      if (gameName.trim().equals("") ) {
                                                                          gameName = getString(R.string.default_game_name);
                                                                      }

                                                                      SharedPreferences settings = getSharedPreferences(MAIN_PREFERNCE_FILE,Context.MODE_PRIVATE);
                                                                      SharedPreferences.Editor editor = settings.edit();
                                                                      editor.putString(F_GAME_NAME,gameName);
                                                                      editor.commit();

                                                                      Promise p = manager.openNewGame(gameName);
                                                                      p.done(new DoneCallback() {
                                                                          public void onDone(Object result) {
                                                                              Log.i(TAG,"Promise:onDone");
                                                                              startActivity(new Intent(theCurrentActivity,PlayerChoosingActivity.class));
                                                                          }
                                                                      }).fail(new FailCallback() {
                                                                          public void onFail(Object rejection) {
                                                                              Log.i(TAG,"Promise:onFail");
                                                                              Toast.makeText(
                                                                                      FirstActivity.this,
                                                                                      "Error in Openning a new game", Toast.LENGTH_SHORT).show();
                                                                          }
                                                                      });

                                                                     }
                                                              }
        );



        findViewById(R.id.join_game).setOnClickListener(new View.OnClickListener() {
                                                                @Override
                                                                public void onClick(View view) {
                                                                    startActivity(new Intent(theCurrentActivity,GameChoosingActivity.class));
                                                                }
                                                            }
        );


    }


    @Override
    public void onResume() {
        super.onResume();
        manager.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        manager.onPause();
    }

}
