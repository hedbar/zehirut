package com.zehirut.user.test2;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.p2p.WifiP2pManager;
import android.net.wifi.p2p.nsd.WifiP2pDnsSdServiceInfo;
import android.util.Log;

import org.jdeferred.Promise;
import org.jdeferred.impl.DeferredObject;
import static com.zehirut.user.test2.Constants.*;

import java.util.HashMap;
import java.util.Set;

public class Manager extends BroadcastReceiver {
    private static final String TAG = Manager.class.getName();
    private final FirstActivity activity;
    private WifiP2pManager p2pManager = null;
    private WifiP2pManager.Channel channel;
    private final IntentFilter intentFilter = new IntentFilter();

    public Manager(FirstActivity firstActivity) {
        this.activity = firstActivity;
    }

    public Promise openNewGame(String gameName) {
        final DeferredObject obj = new DeferredObject();
        Promise p = obj.promise();
        p2pManager.addLocalService(channel, WifiP2pDnsSdServiceInfo.newInstance(gameName, SERVICE_NAME, new HashMap<String, String>()),
                new WifiP2pManager.ActionListener() {
                    @Override
                    public void onSuccess() {
                        Log.i(TAG,"addLocalService: onSuccess ");
                        obj.resolve(null);
                    }
                    @Override
                    public void onFailure(int i) {
                        Log.i(TAG,"addLocalService: onFailure");
                        obj.reject(null);
                    }
                });
        return p;
    }

    public void init() {
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);


        p2pManager = (WifiP2pManager) activity.getSystemService(Context.WIFI_P2P_SERVICE);
        channel = p2pManager.initialize(activity, activity.getMainLooper(), null);


    }

    public void onResume() {
        activity.registerReceiver(this, intentFilter);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        Log.i(TAG,"onReceive : " + action);

        if (intent.getExtras() != null) {
            Set<String> keys = intent.getExtras().keySet();
            for (String key : keys) {
                Log.i(TAG, "\t Key: " + key + " = " + String.valueOf(intent.getExtras().get(key)));
            }
        }


        if (WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION.equals(action)) {
            Log.i(TAG,"TODO");
        } else if (WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION.equals(action)) {
            Log.i(TAG,"TODO");
        } else if (WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION.equals(action)) {
            Log.i(TAG,"TODO");
        } else if (WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION.equals(action)) {
            Log.i(TAG,"TODO");
        } else if (WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION.equals(action)) {
            Log.i(TAG,"TODO");
        }
    }

    public void onPause() {
        activity.unregisterReceiver(this);
    }
}
