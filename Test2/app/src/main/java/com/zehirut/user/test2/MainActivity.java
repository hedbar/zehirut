package com.zehirut.user.test2;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.IntentFilter;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pManager;
import android.net.wifi.p2p.nsd.WifiP2pDnsSdServiceInfo;
import android.net.wifi.p2p.nsd.WifiP2pServiceInfo;
import android.net.wifi.p2p.nsd.WifiP2pServiceRequest;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import static com.zehirut.user.test2.Constants.*;

import java.util.HashMap;
import java.util.Map;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getName();

    private WifiP2pManager manager = null;
    private WifiP2pManager.Channel channel;
    private final IntentFilter intentFilter = new IntentFilter();
    WiFiDirectBroadcastReceiver receiver = null;



    private View mControlsView;
    private final Runnable mShowPart2Runnable = new Runnable() {
        @Override
        public void run() {
            // Delayed display of UI elements
            ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.show();
            }
            mControlsView.setVisibility(View.VISIBLE);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        intentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);

        manager = (WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE);
        channel = manager.initialize(this, getMainLooper(), null);


        setContentView(R.layout.activity_main);


        findViewById(R.id.publish_service).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    manager.addLocalService(channel, WifiP2pDnsSdServiceInfo.newInstance("Device1", "Sevice1", new HashMap<String, String>()),
                            new WifiP2pManager.ActionListener() {
                                @Override
                                public void onSuccess() {
                                    Log.i(TAG,"addLocalService: onSuccess ");
                                }

                                @Override
                                public void onFailure(int i) {
                                    Log.i(TAG,"addLocalService: onFailure");
                                }
                            });
                }
            }
        );

        findViewById(R.id.discover_services).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                manager.setServiceResponseListener(channel, new WifiP2pManager.ServiceResponseListener() {
                    @Override
                    public void onServiceAvailable(int i, byte[] bytes, WifiP2pDevice wifiP2pDevice) {
                        Log.i(TAG,"onServiceAvailable " +String.valueOf(i) + " , " +
                            String.valueOf(bytes) + " , " +
                                String.valueOf(wifiP2pDevice)
                        );
                    }
                });
                manager.setDnsSdResponseListeners(channel, new WifiP2pManager.DnsSdServiceResponseListener() {
                    @Override
                    public void onDnsSdServiceAvailable(String s, String s1, WifiP2pDevice wifiP2pDevice) {
                        Log.i(TAG,"onDnsSdServiceAvailable " +
                                " instanceName = "+ s +
                                " ,registrationType = "+ s1 +
                                " ,device = "+ wifiP2pDevice.deviceName
                        );
                    }
                }, new WifiP2pManager.DnsSdTxtRecordListener() {
                    @Override
                    public void onDnsSdTxtRecordAvailable(String s, Map<String, String> map, WifiP2pDevice wifiP2pDevice) {
                        Log.i(TAG,"onDnsSdTxtRecordAvailable " +
                                " fullDomainName = "+ s +
                                " ,txtRecordMap = "+ String.valueOf(map) +
                                " ,device = "+ wifiP2pDevice.deviceName
                        );
                    }
                });

                WifiP2pServiceRequest req = WifiP2pServiceRequest.newInstance(WifiP2pServiceInfo.SERVICE_TYPE_ALL);
                manager.addServiceRequest(channel, req, new WifiP2pManager.ActionListener() {
                    @Override
                    public void onSuccess() {
                        Log.i(TAG,"addServiceRequest:onSuccess starting discoverServices");
                        manager.discoverServices(channel, new WifiP2pManager.ActionListener() {
                            @Override
                            public void onSuccess() {
                                Log.i(TAG,"discoverServices:onSuccess");

                            }
                            @Override
                            public void onFailure(int i) {
                                Log.i(TAG,"discoverServices:onFailure" + String.valueOf(i));
                            }
                        });

                    }

                    @Override
                    public void onFailure(int i) {
                        Log.i(TAG,"addServiceRequest:onSuccess");

                    }
                });
            }
        });

    }



    @Override
    public void onResume() {
        super.onResume();
        receiver = new WiFiDirectBroadcastReceiver(manager, channel, this);
        registerReceiver(receiver, intentFilter);
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

    }

}
